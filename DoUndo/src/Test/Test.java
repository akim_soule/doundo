package Test;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

public class Test extends Application {
	

	@Override
	public void start(Stage primaryStage) throws Exception {
		Pane pane = new Pane();
		pane.setLayoutX(500);
		pane.setLayoutY(500);
		ObservableList<Node> nodes = FXCollections.observableArrayList();
		nodes.addListener(new ListChangeListener<Node>() {

			@Override
			public void onChanged(Change<? extends Node> c) {
				while (c.next()) {
					if (c.wasAdded()) {
						c.getAddedSubList().stream().forEach(x -> {
							pane.getChildren().add(x);
						});
					}
					if (c.wasRemoved()) {
						c.getRemoved().stream().forEach(x -> {
							pane.getChildren().remove(x);
						});
					}
					
				}
				
			}
			
		});
		Scene scene = new Scene(pane);
		pane.getChildren().add(new Rectangle(10, 10, 10, 10));
		primaryStage.setScene(scene);
		primaryStage.setTitle("Dessin");
		primaryStage.show();
		
		
		scene.addEventFilter(MouseEvent.ANY, new EventHandler<MouseEvent>() {
			Rectangle dragBox = null;
		    @Override
		    public void handle(MouseEvent mouseEvent) {
		        if(mouseEvent.getEventType() == MouseEvent.MOUSE_PRESSED){
		        	dragBox = new Rectangle(0, 0, 10, 10);
		        	System.out.println("clique");
		            dragBox.setVisible(true);
		            dragBox.setFill(Color.AQUA);
		            dragBox.setTranslateX(mouseEvent.getX());
		            dragBox.setTranslateY(mouseEvent.getY());
		        }
		        if(mouseEvent.getEventType() == MouseEvent.MOUSE_DRAGGED && dragBox.isVisible()){
		        	System.out.println("move");
		            dragBox.setWidth(mouseEvent.getX() - dragBox.getTranslateX());
		            dragBox.setHeight(mouseEvent.getY() - dragBox.getTranslateY());
		        }
		        if(mouseEvent.getEventType() == MouseEvent.MOUSE_RELEASED) {
		        	System.out.println("release");
		            dragBox.setVisible(true);
		            nodes.add(dragBox);
		            System.out.println(pane.getChildren().toString());
		        }
		        	
		    }
		});
		
		
//		primaryStage.setResizable(false);
		
		
		
	}
	
	public static void main(String[] args) {
		Application.launch(args);
	}
	
	

}
