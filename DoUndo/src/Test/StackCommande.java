package Test;

import java.util.Stack;

import javafx.scene.Node;

public class StackCommande {
	
	public static StackCommande stackCommande = null;
	private Stack<Couple> undoCommand;
	private Stack<Couple> redoCommand;
	
	public StackCommande() {
		super();
		this.undoCommand = new Stack<Couple>();
		this.redoCommand = new Stack<Couple>();

	}
	
	public static StackCommande getInstance(){
		if (stackCommande == null) {
			stackCommande = new StackCommande();
		}
		return stackCommande;
		
	}
	
	void pushCommandeInUndo(ACTION suprimer, Node node){
		this.undoCommand.push(new Couple(suprimer, node));
	}
	
	Couple popCommande(){
		Couple couple = null;
		if(!this.undoCommand.isEmpty()) {
			couple = this.undoCommand.peek();
			this.redoCommand.push(this.undoCommand.pop());
		}
		return couple;
	}
	
	public Stack<Couple> getUndoCommand() {
		return undoCommand;
	}

	public void setUndoCommand(Stack<Couple> undoCommand) {
		this.undoCommand = undoCommand;
	}

	public Stack<Couple> getRedoCommand() {
		return redoCommand;
	}

	public void setRedoCommand(Stack<Couple> redoCommand) {
		this.redoCommand = redoCommand;
	}

	
	class Couple{
		private ACTION action;
		private Node node;
		
		public Couple(ACTION action, Node node) {
			super();
			this.action = action;
			this.node = node;
		}

		@Override
		public String toString() {
			return "Couple [action=" + action + ", node=" + node + "]";
		}
	
		
	}

}
