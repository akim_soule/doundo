package Test;

import javafx.scene.Node;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;



public class Commande{
	
	public Commande(ACTION action, Node node) {
		switch (action) {
		case AJOUTER:
			StackCommande.getInstance().pushCommandeInUndo(ACTION.SUPRIMER, node);
			break;
		case SUPRIMER:
			StackCommande.getInstance().pushCommandeInUndo(ACTION.AJOUTER, node);
			break;
		default:
			break;
		}
	}
	
	public static void main(String[] args) {
		new Commande(ACTION.AJOUTER, new Rectangle(10, 10, 10, 10));
		new Commande(ACTION.AJOUTER, new Circle(10, 10, 10));
		
		System.out.println(StackCommande.getInstance().getUndoCommand());
		System.out.println(StackCommande.getInstance().getRedoCommand());
		System.out.println(StackCommande.getInstance().popCommande());
		System.out.println(StackCommande.getInstance().popCommande());
		System.out.println(StackCommande.getInstance().popCommande());
		System.out.println(StackCommande.getInstance().getUndoCommand());
		
	}
	

	

}
